const EthSwap = artifacts.require("EthSwap");

const EnergieToken = artifacts.require("EnergieToken");
const Simulation = artifacts.require("Simulation");

module.exports = async function(deployer) {

// deployer l'échange de jeton
  await deployer.deploy(EthSwap);
  const ethSwap = await EthSwap.deployed()

// deployer le jeton ENER
  await deployer.deploy(EnergieToken);
    const token = await EnergieToken.deployed()

  await deployer.deploy(Simulation);
    const simu = await EnergieToken.deployed()

    
// Transfert des jeton vers l'exchange ( 1 milliard)
await token.transfer(Simulation.address, '1000000000000000000000000000')

};
