import React, { Component } from 'react'
import './App.css'
import Web3 from 'web3'
import Main from './Main'
import Simulation from '../abis/Simulation.json' // importe le smart contrat simulation

class App extends Component {




// connection auw web 3 (avec react js)
async componentWillMount(){
  await this.loadWeb3()
  await this.loadBlockchainData()

}

async loadBlockchainData(){
  const web3 = window.web3
  const accounts = await web3.eth.getAccounts()
  //console.log(accounts[0]) // [0] car account est un array
  this.setState({account: accounts})
  console.log(this.state.account) // affiche le compte utilisateur

// "convertion" du smart contrat, afin de le rendre utilisable par du js
const networkId = await web3.eth.net.getId() // dynamise le changement de network ( en f° du network de metamask)
const simulationData = Simulation.networks[networkId]


 const simulation  = new web3.eth.Contract(Simulation.abi,simulationData.address) // instantiation du sc
  // l'abi donne aux js une convertion du smart contrat
  // l'adresse du sc est nécessaire afin de parer a la possibilité d'avoire 2même abi 
  this.setState({simulation})
/*
if (simulationData){ 
   
 
}else{
  window.alert('le smart contrat simulation est introuvable, est-il sur le bon network ??')
}
*/
  const compte = web3.eth.accounts.create() // permet de crée un compte
  console.log(compte.address)

  const compte2 = web3.eth.accounts.create() // permet de crée un compte
  console.log(compte2.address)

//web3.eth.defaultAccount = compte2.address // permet de mettre une adresse nouvellement créé en adresse par défault
console.log (web3.eth.defaultAccount)

//simulation.methods.ajouterMaison(1,2,5).send(this.state.account)
const nbMaison = simulation.methods.connaitre_nombre_Entiter().call()
console.log(nbMaison)

this.setState ({loading: false}) // permet d'enlever l'écran de chargement
}

async loadWeb3(){

    if (window.ethereum) {
        window.web3 = new Web3(window.ethereum);
        try {
            // Request account access if needed
            await window.ethereum.enable();
        } catch (error) {
            // User denied account access...
        }
    }
    // Legacy dapp browsers...
    else if (window.web3) {
        window.web3 = new Web3(window.web3.currentProvider);
    }
    // Non-dapp browsers...
    else {
        console.log('Il manque un portefeuille ethereum afin de se connecter au web3');
    }
}


  constructor(props) {
    super(props)
    this.state = { 
      account:'',
      simulation:{},
      loading:true
    }
  }



  render() {
    let content
    
    if(this.state.loading){
      content = <p id="loader" className="text-center"> chargement ...</p>
    }else{
      content = <Main/>
    }
    return (
      <div>
        <nav className="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">

            Un site Web 3.0
          
        </nav>
        <div className="container-fluid mt-5">
          <div className="row">
            <main role="main" className="col-lg-12 d-flex text-center">
              <div className="content mr-auto ml-auto">
               
                {content}
                
              </div>
            </main>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
