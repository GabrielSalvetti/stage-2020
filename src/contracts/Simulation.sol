pragma solidity ^0.5.0;

contract Simulation {
    int public jour ;
    uint256 public nbEntiter =2;

    // mapping : lier une adresse eth a une structure maison via un id : a terme on utilise l'id et non l'adresse eth 
    mapping (address =>adresse) public Addresse_Id;
    mapping (uint =>Entiter) public entiter;

    address owner;
string public nom = "Simulaleur";

modifier onlyOwner(){
    require (msg.sender == owner);
    _;
}

struct adresse {
	uint id;
	address adresse;
}

    struct Entiter{
        uint id;
        int consomation;
        int production;
        int stockage;
        
       
    }
    
    
    
    constructor() public {
        jour = 0;
        owner = msg.sender;
    }
    
    function ajouterMaison (int consomation, int production, int stockage) public {
        nbEntiter += 1;
        Addresse_Id[msg.sender] = adresse(nbEntiter,msg.sender);
        entiter[nbEntiter] = Entiter (nbEntiter, consomation,production,stockage);
        
    }

	function connaitre_nombre_Entiter() public view returns(uint nbMaison){
		return nbEntiter;
	}


function Caracteristique_de_ma_maison(uint id) public view returns(int prod, int cons, uint id_entiter){
prod= entiter[id].production;
cons= entiter[id].consomation;
id_entiter= entiter[id].id;


}
    
    function Passer_Au_Jour_Suivant() public{
        jour++;
  
    }
        
    
}

/*

pragma solidity ^0.5.0;

contract Simulation {
	string public nom = "Simulaleur";

	    struct TypeEntiter{
        uint id;
        int energie;
        int stockage;
        int production;
        int consomation;
        
    }


    mapping(address => TypeEntiter) public entiter;

}*/