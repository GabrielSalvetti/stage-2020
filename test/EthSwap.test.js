const EthSwap = artifacts.require("EthSwap")
const EnergieToken = artifacts.require("EnergieToken")
const Simulation = artifacts.require("Simulation")


// utilisation de chai : permet des test unitaire en js
require ('chai')
	.use(require('chai-as-promised'))
	.should()

contract('EthSwap', (account) => {

	describe('EthSwap deployment', async () => {
		it('le contrat a un nom', async ()=>{ // declaration du test
			let ethSwap = await EthSwap.new() // instanciation du contrat
			const nom = await ethSwap.name()
			assert.equal(nom,'EthSwap Instant Exchange' )
		})

	})

})

contract('Simulation', (account) => {

	describe('Simulation deployment', async () => {
		it('le contrat a un nom', async ()=>{ // declaration du test
			let simu = await Simulation.new() // instanciation du contrat
			const nom = await simu.nom()
			assert.equal(nom,'Simulaleur' )
		})

	})

})